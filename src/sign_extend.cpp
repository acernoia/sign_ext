#include <systemc.h>
#include "sign_extend.hpp"

using namespace std;

void sign_extend::extend()
{
   sc_uint<7> interno;
   while(true)
   {
     wait(); 
     interno=ingresso->read();
     if (interno[6]==0)
        uscita->write(ingresso->read());
     else
        uscita->write(65408|ingresso->read());
     //NOTA: 65408   = 1111 1111 1000 0000
   }

}
