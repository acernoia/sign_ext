#ifndef SIGN_EXTEND_HPP
#define SIGN_EXTEND_HPP

SC_MODULE(sign_extend)
{
   sc_in<sc_uint<7> > ingresso;
   sc_out<sc_uint<16> >  uscita;

   SC_CTOR(sign_extend)
   {
     SC_THREAD(extend);
     sensitive << ingresso;
   }
   private:
   void extend ();
};


#endif
